
# build locally

```shell
antora --fetch --redirect-facility=gitlab --to-dir=public antora-playbook.yml
```

# run locally

```shell
npm i -g http-server
http-server public -c-1
```

http://127.0.0.1:8080